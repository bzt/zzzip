#include <zzz.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <time.h>

int main(int argc, char **argv)
{
    zzz_entity_t *ent;
    int ret;
    void *ctx = zzz_create_file("a.zzz");
    uint8_t *mem = NULL;
    uint64_t size;
    char buf[4096];
    char *content = "hello world\nmasik sor\nmeg valami hogy legyen hosszabb\nhello world\nmasik sor\nmeg valami hogy legye0123456789\n";
    char *password = "valami";
    (void)argc;
    (void)argv;
    /* compress per entity */
    zzz_entity_file(ctx, "valami.txt", 1, strlen(content));
    zzz_entity_mtime_t(ctx, time(NULL));
    zzz_entity_extra_mime(ctx, "text/plain");
    zzz_entity_data(ctx, (uint8_t*)content, strlen(content));
    zzz_entity_flush(ctx);

    zzz_entity_symlink(ctx, "link.txt", "valami.txt");
    zzz_entity_extra_mime(ctx, "text/plain");
    zzz_entity_flush(ctx);

    zzz_entity_dir(ctx, "directory");
    zzz_entity_flush(ctx);

    zzz_finish(ctx, &mem, NULL);
    free(mem);

printf("\n\n");

    ctx = zzz_open_file("a.zzz");
    if(!ctx) { printf("unable to read\n"); return 0; }
    do {
        ret = zzz_read_header(ctx, &ent);
        printf("read header %d\n",ret);
        switch(ret) {
            case ZZZ_END: printf("end.\n"); break;
            case ZZZ_ENCRYPTED:
                ret = zzz_decrypt(ctx, (uint8_t*)password, strlen(password));
                continue;
            case ZZZ_OK:
                printf("fn '%s'\ntype %02x uncompressed %ld compressed %ld filt %d %d\n", ent->filename, ent->header.type,
                    ent->header.uncompressed, ent->header.contentsize, ent->filter[0].method, ent->filter[1].method);
                if(ent->header.uncompressed) {
                    size = ent->header.uncompressed;
                    ret = zzz_read_data(ctx, (uint8_t*)&buf, &size);
                    printf("data read uncomp %ld ret %d\n", size, ret);
                    ret = ZZZ_OK;
                }
                break;
            default:
                printf("error %d\n", ret);
                break;
        }
    } while(ret == ZZZ_OK);
    zzz_close(ctx);
    return 0;
}
