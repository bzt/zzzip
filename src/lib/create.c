/*
 * ZZZip/src/lib/create.c
 *
 * Copyright (C) 2021 bzt (bztsrc@gitlab)
 *
 * Permission is hereby granted, free of charge, to any person
 * obtaining a copy of this software and associated documentation
 * files (the "Software"), to deal in the Software without
 * restriction, including without limitation the rights to use, copy,
 * modify, merge, publish, distribute, sublicense, and/or sell copies
 * of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
 * HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
 * WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 *
 * @brief ZZZip library archive creation functions
 *
 */

#include "internal.h"

/**
 * Helper to add ACLs
 */
static int _zzz_entity_extra_acl(zzz_ctx_t *zzz, int type, uint8_t *acl, int size)
{
    int i, sep = 0;
    if(zzz->entity.numextra && zzz->entity.extra[zzz->entity.numextra - 1].type == type) {
        i = zzz->entity.numextra - 1;
    } else {
        for(i = 0; i < zzz->entity.numextra && zzz->entity.extra[i].type != type; i++);
        if(i == zzz->entity.numextra) {
            zzz->entity.extra = (zzz_attr_t*)realloc(zzz->entity.extra, (zzz->entity.numextra + 1) * sizeof(zzz_attr_t));
            if(!zzz->entity.extra) return ZZZ_ERR_NOMEM;
            zzz->entity.extra[zzz->entity.numextra].type = type;
            zzz->entity.extra[zzz->entity.numextra].size = 0;
            zzz->entity.extra[zzz->entity.numextra].attr.any = NULL;
            zzz->entity.numextra++;
        }
    }
    if(size < 2) { sep = 1; size = strlen((char*)acl) + 1; }
    zzz->entity.extra[i].attr.any = (uint8_t*)realloc(zzz->entity.extra[i].attr.any,
        zzz->entity.extra[zzz->entity.numextra].size + size + 2);
    if(!zzz->entity.extra[i].attr.any) return ZZZ_ERR_NOMEM;
    if(sep && zzz->entity.extra[i].size &&
        zzz->entity.extra[i].attr.any[zzz->entity.extra[i].size - 1] != '\n')
            zzz->entity.extra[i].attr.any[zzz->entity.extra[i].size++] = '\n';
    memcpy(zzz->entity.extra[i].attr.any + zzz->entity.extra[i].size, acl, size);
    if(sep && zzz->entity.extra[i].attr.any[zzz->entity.extra[i].size + size - 1] != '\n') {
        zzz->entity.extra[i].attr.any[zzz->entity.extra[i].size + size++] = '\n';
        zzz->entity.extra[i].attr.any[zzz->entity.extra[i].size + size] = 0;
    }
    zzz->entity.extra[i].size += size;
    return ZZZ_OK;
}

/**
 * Helper to encrypt a buffer
 */
static void _zzz_enc(zzz_ctx_t *zzz, uint8_t *buf, uint64_t size)
{
    int i;
    for(i = zzz->enc.size - 1; i >= 0; i--) {
        switch(zzz->enc.enctype[i]) {
            case ZZZ_ENC_AES_256_CBC:
                if(!buf || !size) { aes_reset(zzz, i); }
                else              { aes_enc(zzz, i, buf, size); }
                break;
            case ZZZ_ENC_SHA_256_XOR:
                if(!buf || !size) { sha_reset(zzz, i); }
                else              { sha_enc(zzz, i, buf, size); }
                break;
            default: break;
        }
    }
}

/**
 * Helper to write to output
 */
static int _zzz_write(zzz_ctx_t *zzz, void *buf, uint64_t size)
{
    ZSTD_inBuffer zi;
    if(zzz->ae) {
        if(!zzz->cmp) {
            zzz->cmp = (uint8_t*)malloc(ZZZ_BUFSIZE);
            if(!zzz->cmp) return ZZZ_ERR_NOMEM;
            zzz->zo.dst = zzz->cmp;
            zzz->zo.size = ZZZ_BUFSIZE;
        }
        zi.src = buf; zi.size = size; zi.pos = 0;
        ZSTD_compressStream2(zzz->ae, &zzz->zo , &zi, &zzz->end == buf ? ZSTD_e_end : ZSTD_e_continue);
        buf = zzz->cmp;
        size = zzz->zo.pos;
    }
    if(!size) return ZZZ_OK;
    zzz->zo.pos = 0;
    if(zzz->mem) {
        if(zzz->apos + size >= zzz->mpos) {
            zzz->mpos += ZZZ_BUFSIZE;
            zzz->mem = (uint8_t*)realloc(zzz->mem, zzz->mpos);
            if(!zzz->mem) return ZZZ_ERR_NOMEM;
        }
        memcpy(zzz->mem + zzz->apos, buf, size);
    } else if(zzz->fd != -1) {
        zzz_swrite(zzz->fd, buf, size);
    } else {
        zzz_fwrite(zzz->f, buf, size);
    }
    zzz->apos += size;
    return ZZZ_OK;
}

/**
 * Helper to add a file name
 */
static int _zzz_addfn(zzz_ctx_t *zzz, char *fn, int isdir)
{
    int i;
    if(!fn) return 0;
    if(*fn == '/') fn++;
    if(!*fn) return 0;
    _zzz_entity_free(&zzz->entity);
    zzz->pos = 0; zzz->cmpcrc = 0; zzz->oneround = zzz->chksiz = 0; zzz->end.numblk++;
    zzz->entity.header.namelen = strlen(fn) + 1;
    if(zzz->entity.header.namelen > 32766) return 0;
    zzz->entity.filename = (char*)malloc(zzz->entity.header.namelen + isdir);
    if(!zzz->entity.filename) return 0;
    memcpy(zzz->entity.filename, fn, zzz->entity.header.namelen);
    if(isdir && zzz->entity.filename[zzz->entity.header.namelen - 2] != '/') {
        zzz->entity.filename[zzz->entity.header.namelen - 1] = '/';
        zzz->entity.filename[zzz->entity.header.namelen++] = 0;
    }
    if(zzz->enc.passwdcrc) {
        _zzz_enc(zzz, NULL, 0);
        if(!zzz->enchdr) {
            zzz->enc.passwdcrc = le32(zzz->enc.passwdcrc);
            memcpy(zzz->enc.magic, ZZZ_MAGIC, 4);
            i = zzz->enc.size + 13;
            zzz->enc.size = le16(i);
            for(zzz->enc.padding = 1; (1 << zzz->enc.padding) < zzz->pad; zzz->enc.padding++);
            zzz->end.numblk++;
            _zzz_write(zzz, &zzz->enc, i);
            zzz->end.acrc = crc32_z(zzz->end.acrc, (uint8_t*)&zzz->enc, i);
            zzz->enc.passwdcrc = le32(zzz->enc.passwdcrc);
            zzz->enc.size = i - 13;
            zzz->enchdr = 1;
        }
    }
    return 1;
}

/**
 * Helper to write (potentially encrypted) data
 */
static int _zzz_write_enc(zzz_ctx_t *zzz, uint8_t *buf, uint64_t size)
{
    int ret;
    uint64_t l;
    uint8_t *buf2 = NULL;
    if(zzz->enc.passwdcrc) {
        if(zzz->pad > 1) {
            l = zzz->pad - zzz->chksiz;
            if(buf == (uint8_t*)&zzz->chunk) {
                if(!zzz->chksiz) return ZZZ_OK;
                memset(zzz->chunk + zzz->chksiz, 0, l);
                _zzz_enc(zzz, zzz->chunk, zzz->pad);
                zzz->cmpcrc = crc32_z(zzz->cmpcrc, zzz->chunk, zzz->pad);
                return _zzz_write(zzz, zzz->chunk, zzz->pad);
            }
            if(zzz->chksiz + size < (uint64_t)zzz->pad) {
                memcpy(zzz->chunk + zzz->chksiz, buf, size);
                zzz->chksiz += size;
                return ZZZ_OK;
            }
            if(zzz->chksiz) {
                memcpy(zzz->chunk + zzz->chksiz, buf, l);
                _zzz_enc(zzz, zzz->chunk, zzz->pad);
                zzz->cmpcrc = crc32_z(zzz->cmpcrc, zzz->chunk, zzz->pad);
                _zzz_write(zzz, zzz->chunk, zzz->pad);
                buf += l; size -= l;
            }
            zzz->chksiz = (uint8_t)size & (zzz->pad - 1);
            if(zzz->chksiz) {
                memcpy(zzz->chunk, buf + size - zzz->chksiz, zzz->chksiz);
                size -= zzz->chksiz;
            }
        }
        buf2 = (uint8_t*)malloc(size);
        if(!buf2) return ZZZ_ERR_NOMEM;
        memcpy(buf2, buf, size);
        _zzz_enc(zzz, buf2, size);
        buf = buf2;
    }
    zzz->cmpcrc = crc32_z(zzz->cmpcrc, buf, size);
    ret = _zzz_write(zzz, buf, size);
    if(buf2) free(buf2);
    return ret;
}

/**
 * Helper to generate header
 */
static int _zzz_header(zzz_ctx_t *zzz)
{
    uint8_t *ptr;
    zzz_block_t *blk;
    int i, j, l, s, o;
    if(!zzz) return ZZZ_ERR_BADINP;
    if(!zzz->hdr)
        zzz->hdr = (uint8_t*)malloc(ZZZ_HEADER_MAXSIZE);
    if(!zzz->hdr) return ZZZ_ERR_NOMEM;
    s = o = sizeof(zzz_block_t) + ZZZ_NUM_FILTER(zzz->entity.header.type) * sizeof(uint16_t);
    ptr = zzz->hdr + s;
    for(i = 0; i < zzz->entity.numextra; i++) {
        if(s + 4 + zzz->entity.extra[i].size > ZZZ_HEADER_MAXSIZE) return ZZZ_ERR_HDRTOOBIG;
        ((zzz_extra_t*)ptr)->type = zzz->entity.extra[i].type;
#if __BYTE_ORDER == __LITTLE_ENDIAN
        ((zzz_extra_t*)ptr)->rev = 0;
#else
        ((zzz_extra_t*)ptr)->rev = (zzz->entity.extra[i].type >= 0x10 ? 0x80 : 0);
#endif
        ((zzz_extra_t*)ptr)->size = le16(zzz->entity.extra[i].size + 4);
        ptr += sizeof(zzz_extra_t);
        switch(zzz->entity.extra[i].type) {
            case ZZZ_EXTRA_META:
                if(zzz->entity.extra[i].attr.meta.data) {
                    for(j = 0; j < zzz->entity.extra[i].attr.meta.len; j++) {
                        *((uint16_t*)ptr) = le16(zzz->entity.extra[i].attr.meta.data[j].size); ptr += 2;
                        l = strlen(zzz->entity.extra[i].attr.meta.data[j].name) + 1;
                        memcpy(ptr, zzz->entity.extra[i].attr.meta.data[j].name, l); ptr += l;
                        l = zzz->entity.extra[i].attr.meta.data[j].size;
                        memcpy(ptr, zzz->entity.extra[i].attr.meta.data[j].data, l); ptr += l;
                    }
                }
                break;
            case ZZZ_EXTRA_TIME:
                *((uint64_t*)ptr) = le64(zzz->entity.extra[i].attr.time.mtime); ptr += 8;
                if(zzz->entity.extra[i].size > 8) {
                    *((uint64_t*)ptr) = le64(zzz->entity.extra[i].attr.time.atime); ptr += 8;
                    if(zzz->entity.extra[i].size > 16) {
                        *((uint64_t*)ptr) = le64(zzz->entity.extra[i].attr.time.ctime); ptr += 8;
                        if(zzz->entity.extra[i].size > 24) {
                            *((uint64_t*)ptr) = le64(zzz->entity.extra[i].attr.time.btime); ptr += 8;
                        }
                    }
                }
                break;
            case ZZZ_EXTRA_ACCESS:
                *((uint32_t*)ptr) = le32(zzz->entity.extra[i].attr.access.mode); ptr += 4;
                *((uint64_t*)ptr) = le64(zzz->entity.extra[i].attr.access.uid); ptr += 8;
                *((uint64_t*)ptr) = le64(zzz->entity.extra[i].attr.access.gid); ptr += 8;
                l = strlen(zzz->entity.extra[i].attr.access.user) + 1;
                memcpy(ptr, zzz->entity.extra[i].attr.access.user, l); ptr += l;
                l = strlen(zzz->entity.extra[i].attr.access.group) + 1;
                memcpy(ptr, zzz->entity.extra[i].attr.access.group, l); ptr += l;
                break;
            default:
                memcpy(ptr, zzz->entity.extra[i].attr.any, zzz->entity.extra[i].size);
                ptr += zzz->entity.extra[i].size;
                break;
        }
        s += 4 + zzz->entity.extra[i].size;
    }
    if(zzz->enc.passwdcrc) {
        if(zzz->pad > 1) {
            j = zzz->pad - ((s - o) & (zzz->pad - 1));
            if(j < zzz->pad) {
                if(j < 4) j += zzz->pad;
                ((zzz_extra_t*)ptr)->type = ZZZ_EXTRA_PAD;
                ((zzz_extra_t*)ptr)->rev = 0;
                ((zzz_extra_t*)ptr)->size = le16(j);
                ptr += sizeof(zzz_extra_t);
                memset(ptr, 0, j - 4);
                s += j;
            }
        }
        _zzz_enc(zzz, zzz->hdr + o, s - o);
    }
    memcpy(zzz->hdr, &zzz->entity.header, sizeof(zzz_block_t));
    blk = (zzz_block_t*)zzz->hdr;
    memcpy(&blk->magic, ZZZ_MAGIC, 4);
    blk->size = le16(s & 0xFFFF);
    blk->namelen = le16(((s & 0x10000) >> 1) | (zzz->entity.header.namelen & 0x7FFF));
    blk->myear = le16(zzz->entity.header.myear);
    blk->uncompressed = le64(zzz->entity.header.uncompressed);
    blk->contentsize = le64(zzz->entity.header.contentsize);
    i = ZZZ_NUM_FILTER(zzz->entity.header.type);
    if(i > 0)
        for(--i, ptr = zzz->hdr + sizeof(zzz_block_t); i >= 0; i--) {
            *ptr++ = zzz->entity.filter[i].method;
            *ptr++ = zzz->entity.filter[i].level;
        }
    zzz->hdrsize = s;
    zzz->hpos = zzz->apos;
    zzz->entity.blkcrc = crc32_z(0, zzz->hdr, s);
    zzz->entity.blkcrc = crc32_z(zzz->entity.blkcrc, (uint8_t*)zzz->entity.filename, zzz->entity.header.namelen);
    if((i = _zzz_write(zzz, zzz->hdr, s)) != ZZZ_OK) return i;
    return _zzz_write(zzz, zzz->entity.filename, zzz->entity.header.namelen);
}

/**
 * Create an archive. Fn is an UTF-8 filename.
 */
void *zzz_create_file(char *fn)
{
    zzz_ctx_t *ctx = malloc(sizeof(zzz_ctx_t));
    if(!ctx) return NULL;
    memset(ctx, 0, sizeof(zzz_ctx_t));
    ctx->f = zzz_fopen(fn, 1);
    if(!ctx->f) {
        free(ctx);
        return NULL;
    }
    ctx->fd = -1;
    return (void*)ctx;
}

/**
 * Create an archive in a stream
 */
void *zzz_create_stream(int fd)
{
    zzz_ctx_t *ctx = malloc(sizeof(zzz_ctx_t));
    if(!ctx) return NULL;
    memset(ctx, 0, sizeof(zzz_ctx_t));
    ctx->fd = fd;
    return (void*)ctx;
}

/**
 * Create an in-memory archive
 */
void *zzz_create_mem()
{
    zzz_ctx_t *ctx = malloc(sizeof(zzz_ctx_t));
    if(!ctx) return NULL;
    memset(ctx, 0, sizeof(zzz_ctx_t));
    ctx->mem = malloc(ZZZ_BUFSIZE);
    if(!ctx->mem) { free(ctx); return NULL; }
    ctx->mpos = ZZZ_BUFSIZE;
    ctx->fd = -1;
    return (void*)ctx;
}

/**
 * Set overall archive compression
 */
int zzz_compression(void *ctx, int method, int level)
{
    zzz_ctx_t *zzz = (zzz_ctx_t*)ctx;
    /* we only support zstd for now, but could be other in the future */
    if(!zzz || method != ZZZ_FILTER_ZSTD || zzz->enc.passwdcrc) return ZZZ_ERR_BADINP;
    zzz->ae = ZSTD_createCCtx();
    if(!zzz->ae) return ZZZ_ERR_NOMEM;
    ZSTD_CCtx_setParameter(zzz->ae, ZSTD_c_compressionLevel, level);
    zzz->comp.method = method;
    zzz->comp.level = level;
    return ZZZ_OK;
}

/**
 * Set encryption method and password
 */
int zzz_encrypt(void *ctx, int method, uint8_t *password, int pwdlen)
{
    uint32_t crc;
    zzz_ctx_t *zzz = (zzz_ctx_t*)ctx;
    if(!zzz || !password || pwdlen < 1 || zzz->enc.size > MAX_ENC_FILTERS) return ZZZ_ERR_BADINP;
    crc = crc32_z(0, password, pwdlen);
    if(zzz->enc.passwdcrc && zzz->enc.passwdcrc != crc) return ZZZ_ERR_BADINP;
    switch(method) {
        case ZZZ_ENC_AES_256_CBC: aes_init(zzz, password, pwdlen); break;
        case ZZZ_ENC_SHA_256_XOR: sha_init(zzz, password, pwdlen); break;
        default: return ZZZ_ERR_BADINP;
    }
    zzz->enc.enctype[zzz->enc.size++] = method;
    zzz->enc.passwdcrc = crc;
    return ZZZ_OK;
}

/**
 * Add a file to the archive. Should be followed by zzz_entity_data() calls
 */
int zzz_entity_file(void *ctx, char *fn, int text, int64_t size)
{
    zzz_ctx_t *zzz = (zzz_ctx_t*)ctx;
    if(!zzz || !fn || !*fn || !_zzz_addfn(ctx, fn, 0)) return ZZZ_ERR_BADINP;
    zzz->entity.header.type = ZZZ_TYPE_REG;
    zzz->entity.header.uncompressed = size;
    zzz->end.filetypes |= (1 << (ZZZ_TYPE_REG >> 4));
    if(text) {
        zzz->entity.filter[zzz->entity.header.type++].method = ZZZ_FILTER_ASCII;
    }
    if(!zzz->ae) {
        zzz->ee = ZSTD_createCCtx();
        if(!zzz->ee) return ZZZ_ERR_NOMEM;
        ZSTD_CCtx_setParameter(zzz->ee, ZSTD_c_compressionLevel, 22);
        if(!zzz->cmp)
            zzz->cmp = (uint8_t*)malloc(ZZZ_BUFSIZE);
        if(!zzz->cmp) return ZZZ_ERR_NOMEM;
    }
    return ZZZ_OK;
}

/**
 * Add a hard link to the archive
 */
int zzz_entity_hardlink(void *ctx, char *fn, char *target)
{
    zzz_ctx_t *zzz = (zzz_ctx_t*)ctx;
    if(!zzz || !fn || !*fn || !target || !*target || !_zzz_addfn(ctx, fn, 0)) return ZZZ_ERR_BADINP;
    zzz->entity.header.type = ZZZ_TYPE_LNK;
    zzz->entity.header.contentsize = strlen(target) + 1;
    zzz->entity.target = (char*)malloc(zzz->entity.header.contentsize);
    if(!zzz->entity.target) return ZZZ_ERR_NOMEM;
    memcpy(zzz->entity.target, target, zzz->entity.header.contentsize);
    zzz->end.filetypes |= (1 << (ZZZ_TYPE_SYM >> 4));
    return ZZZ_OK;
}

/**
 * Add a symbolic link to the archive
 */
int zzz_entity_symlink(void *ctx, char *fn, char *target)
{
    zzz_ctx_t *zzz = (zzz_ctx_t*)ctx;
    if(!zzz || !fn || !*fn || !target || !*target || !_zzz_addfn(ctx, fn, 0)) return ZZZ_ERR_BADINP;
    zzz->entity.header.type = ZZZ_TYPE_SYM;
    zzz->entity.header.contentsize = strlen(target) + 1;
    zzz->entity.target = (char*)malloc(zzz->entity.header.contentsize);
    if(!zzz->entity.target) return ZZZ_ERR_NOMEM;
    memcpy(zzz->entity.target, target, zzz->entity.header.contentsize);
    zzz->end.filetypes |= (1 << (ZZZ_TYPE_SYM >> 4));
    return ZZZ_OK;
}

/**
 * Add a union to the archive. Must be followed by zzz_entity_union_target() calls
 */
int zzz_entity_union(void *ctx, char *fn)
{
    zzz_ctx_t *zzz = (zzz_ctx_t*)ctx;
    if(!zzz || !fn || !*fn || !_zzz_addfn(ctx, fn, 0)) return ZZZ_ERR_BADINP;
    zzz->entity.header.type = ZZZ_TYPE_UNI;
    zzz->end.filetypes |= (1 << (ZZZ_TYPE_UNI >> 4));
    return ZZZ_OK;
}

/**
 * Add a union target to the archive
 */
int zzz_entity_union_target(void *ctx, char *target)
{
    uint64_t l;
    zzz_ctx_t *zzz = (zzz_ctx_t*)ctx;
    if(!zzz || zzz->entity.header.type != ZZZ_TYPE_UNI || !target || !*target) return ZZZ_ERR_BADINP;
    l = strlen(target) + 1;
    zzz->entity.target = (char*)realloc(zzz->entity.target, zzz->entity.header.contentsize + l);
    if(!zzz->entity.target) return ZZZ_ERR_NOMEM;
    memcpy(zzz->entity.target + zzz->entity.header.contentsize, target, l);
    zzz->entity.header.contentsize += l;
    return ZZZ_OK;
}

/**
 * Add a character device to the archive
 */
int zzz_entity_chrdev(void *ctx, char *fn, uint32_t devmaj, uint32_t devmin)
{
    zzz_ctx_t *zzz = (zzz_ctx_t*)ctx;
    if(!zzz || !fn || !*fn || !_zzz_addfn(ctx, fn, 0)) return ZZZ_ERR_BADINP;
    zzz->entity.header.type = ZZZ_TYPE_CHR;
    zzz->entity.header.contentsize = 8;
    zzz->entity.target = (char*)malloc(zzz->entity.header.contentsize);
    if(!zzz->entity.target) return ZZZ_ERR_NOMEM;
    memcpy(zzz->entity.target, &devmaj, 4);
    memcpy(zzz->entity.target + 4, &devmin, 4);
    zzz->end.filetypes |= (1 << (ZZZ_TYPE_CHR >> 4));
    return ZZZ_OK;
}

/**
 * Add a block device to the archive
 */
int zzz_entity_blkdev(void *ctx, char *fn, uint32_t devmaj, uint32_t devmin)
{
    zzz_ctx_t *zzz = (zzz_ctx_t*)ctx;
    if(!zzz || !fn || !*fn || !_zzz_addfn(ctx, fn, 0)) return ZZZ_ERR_BADINP;
    zzz->entity.header.type = ZZZ_TYPE_BLK;
    zzz->entity.header.contentsize = 8;
    zzz->entity.target = (char*)malloc(zzz->entity.header.contentsize);
    if(!zzz->entity.target) return ZZZ_ERR_NOMEM;
    memcpy(zzz->entity.target, &devmaj, 4);
    memcpy(zzz->entity.target + 4, &devmin, 4);
    zzz->end.filetypes |= (1 << (ZZZ_TYPE_BLK >> 4));
    return ZZZ_OK;
}

/**
 * Add a directory to the archive
 */
int zzz_entity_dir(void *ctx, char *fn)
{
    zzz_ctx_t *zzz = (zzz_ctx_t*)ctx;
    if(!zzz || !fn || !*fn || !_zzz_addfn(ctx, fn, 1)) return ZZZ_ERR_BADINP;
    zzz->entity.header.type = ZZZ_TYPE_DIR;
    zzz->end.filetypes |= (1 << (ZZZ_TYPE_DIR >> 4));
    return ZZZ_OK;
}

/**
 * Add a named fifo to the archive
 */
int zzz_entity_fifo(void *ctx, char *fn)
{
    zzz_ctx_t *zzz = (zzz_ctx_t*)ctx;
    if(!zzz || !fn || !*fn || !_zzz_addfn(ctx, fn, 0)) return ZZZ_ERR_BADINP;
    zzz->entity.header.type = ZZZ_TYPE_FIFO;
    zzz->end.filetypes |= (1 << (ZZZ_TYPE_FIFO >> 4));
    return ZZZ_OK;
}

/**
 * Add canonical file modification date and time
 */
int zzz_entity_mtime(void *ctx, uint16_t year, uint8_t mon, uint8_t day, uint8_t hour, uint8_t min, uint8_t sec)
{
    zzz_ctx_t *zzz = (zzz_ctx_t*)ctx;
    if(!zzz || mon < 1 || mon > 12 || day < 1 || day > 31 || hour > 23 || min > 59 || sec > 60) return ZZZ_ERR_BADINP;
    zzz->entity.header.myear = year;
    zzz->entity.header.mmon = mon;
    zzz->entity.header.mday = day;
    zzz->entity.header.mhour = hour;
    zzz->entity.header.mmin = min;
    zzz->entity.header.msec = sec;
    return ZZZ_OK;
}

/**
 * Add canonical file modification date and time from timestamp
 */
int zzz_entity_mtime_t(void *ctx, uint64_t t)
{
    struct tm *tm = gmtime((time_t*)&t);
    return zzz_entity_mtime(ctx, tm->tm_year + 1900, tm->tm_mon + 1, tm->tm_mday, tm->tm_hour, tm->tm_min, tm->tm_sec);
}

/**
 * Add a comment to the entity
 */
int zzz_entity_extra_comment(void *ctx, char *comment)
{
    if(!ctx || !comment || !*comment) return ZZZ_ERR_BADINP;
    return zzz_entity_extra((zzz_ctx_t*)ctx, ZZZ_EXTRA_COMMENT, (uint8_t*)comment, strlen(comment) + 1);
}

/**
 * Add a mime type (optionally with ";charset=X" suffix) to the entity
 */
int zzz_entity_extra_mime(void *ctx, char *mime)
{
    if(!ctx || !mime || !*mime) return ZZZ_ERR_BADINP;
    return zzz_entity_extra((zzz_ctx_t*)ctx, ZZZ_EXTRA_MIME, (uint8_t*)mime, strlen(mime) + 1);
}

/**
 * Add a PNG or SVG icon to the entity
 */
int zzz_entity_extra_icon(void *ctx, uint8_t *buf, uint16_t size)
{
    if(!ctx || !buf || !size) return ZZZ_ERR_BADINP;
    return zzz_entity_extra((zzz_ctx_t*)ctx, ZZZ_EXTRA_ICON, buf, size);
}

/**
 * Add a meta info (xattr) to the entity, may be called multiple times
 */
int zzz_entity_extra_meta(void *ctx, char *key, uint8_t *buf, uint16_t size)
{
    int i, l;
    zzz_meta_t *meta;
    zzz_ctx_t *zzz = (zzz_ctx_t*)ctx;
    if(!zzz || !key || !*key || !buf || !size) return ZZZ_ERR_BADINP;
    if(zzz->entity.numextra && zzz->entity.extra[zzz->entity.numextra - 1].type == ZZZ_EXTRA_META) {
        i = zzz->entity.numextra - 1;
    } else {
        for(i = 0; i < zzz->entity.numextra && zzz->entity.extra[i].type != ZZZ_EXTRA_META; i++);
        if(i == zzz->entity.numextra) {
            zzz->entity.extra = (zzz_attr_t*)realloc(zzz->entity.extra, (zzz->entity.numextra + 1) * sizeof(zzz_attr_t));
            if(!zzz->entity.extra) return ZZZ_ERR_NOMEM;
            zzz->entity.extra[zzz->entity.numextra].type = ZZZ_EXTRA_META;
            zzz->entity.extra[zzz->entity.numextra].size = 0;
            zzz->entity.extra[zzz->entity.numextra].attr.meta.len = 0;
            zzz->entity.extra[zzz->entity.numextra].attr.meta.data = NULL;
            zzz->entity.numextra++;
        }
    }
    zzz->entity.extra[i].attr.meta.data = (zzz_meta_t*)realloc(
        zzz->entity.extra[i].attr.meta.data, (zzz->entity.extra[i].attr.meta.len + 1) * sizeof(zzz_meta_t));
    if(zzz->entity.extra[i].attr.meta.data) {
        l = strlen(key) + 1;
        meta = &zzz->entity.extra[i].attr.meta.data[zzz->entity.extra[i].attr.meta.len];
        meta->name = (char*)malloc(l);
        if(meta->name) {
            memcpy(meta->name, key, l);
            meta->data = (uint8_t*)malloc(size);
            if(meta->data) {
                memcpy(meta->data, buf, size);
                zzz->entity.extra[i].attr.meta.len++;
                zzz->entity.extra[i].size += 4 + l + size;
            } else {
                free(meta->name);
                meta->name = NULL;
            }
        }
    }
    return ZZZ_OK;
}

/**
 * Add a nanosec precision time spec to the entity (btime is birth time, date and time of file creation)
 */
int zzz_entity_extra_time(void *ctx, uint64_t mtime, uint64_t atime, uint64_t ctime, uint64_t btime)
{
    zzz_ctx_t *zzz = (zzz_ctx_t*)ctx;
    if(!zzz || !mtime) return ZZZ_ERR_BADINP;
    zzz->entity.extra = (zzz_attr_t*)realloc(zzz->entity.extra, (zzz->entity.numextra + 1) * sizeof(zzz_attr_t));
    if(!zzz->entity.extra) return ZZZ_ERR_NOMEM;
    zzz->entity.extra[zzz->entity.numextra].size = 8;
    zzz->entity.extra[zzz->entity.numextra].type = ZZZ_EXTRA_TIME;
    zzz->entity.extra[zzz->entity.numextra].attr.time.mtime = mtime;
    if(atime || ctime || btime) {
        zzz->entity.extra[zzz->entity.numextra].size += 8;
        zzz->entity.extra[zzz->entity.numextra].attr.time.atime = atime;
        if(ctime || btime) {
            zzz->entity.extra[zzz->entity.numextra].size += 8;
            zzz->entity.extra[zzz->entity.numextra].attr.time.ctime = ctime;
            if(btime) {
                zzz->entity.extra[zzz->entity.numextra].size += 8;
                zzz->entity.extra[zzz->entity.numextra].attr.time.btime = btime;
            }
        }
    }
    zzz->entity.numextra++;
    return ZZZ_OK;
}

/**
 * Add POSIX access rights to the entity
 */
int zzz_entity_extra_access(void *ctx, uint32_t mode, char *user, uint64_t uid, char *group, uint64_t gid)
{
    int l;
    zzz_ctx_t *zzz = (zzz_ctx_t*)ctx;
    if(!zzz || !user || !*user || !group || !*group) return ZZZ_ERR_BADINP;
    zzz->entity.extra = (zzz_attr_t*)realloc(zzz->entity.extra, (zzz->entity.numextra + 1) * sizeof(zzz_attr_t));
    if(!zzz->entity.extra) return ZZZ_ERR_NOMEM;
    l = strlen(user) + 1;
    zzz->entity.extra[zzz->entity.numextra].attr.access.user = (char*)malloc(l);
    if(!zzz->entity.extra[zzz->entity.numextra].attr.access.user) return ZZZ_ERR_NOMEM;
    memcpy(zzz->entity.extra[zzz->entity.numextra].attr.access.user, user, l);
    zzz->entity.extra[zzz->entity.numextra].size = l;
    l = strlen(group) + 1;
    zzz->entity.extra[zzz->entity.numextra].attr.access.group = (char*)malloc(l);
    if(!zzz->entity.extra[zzz->entity.numextra].attr.access.group) {
        free(zzz->entity.extra[zzz->entity.numextra].attr.access.user);
        return ZZZ_ERR_NOMEM;
    }
    memcpy(zzz->entity.extra[zzz->entity.numextra].attr.access.group, group, l);
    zzz->entity.extra[zzz->entity.numextra].size += l + 4 + 4 + 8 + 8;
    zzz->entity.extra[zzz->entity.numextra].type = ZZZ_EXTRA_ACCESS;
    zzz->entity.extra[zzz->entity.numextra].attr.access.mode = mode;
    zzz->entity.extra[zzz->entity.numextra].attr.access.uid = uid;
    zzz->entity.extra[zzz->entity.numextra].attr.access.gid = gid;
    zzz->entity.numextra++;
    return ZZZ_OK;
}

/**
 * Add NFSv4 ACL to the entity, may be called multiple times
 */
int zzz_entity_extra_acl_nfs(void *ctx, char *acl)
{
    if(!ctx || !acl || !*acl) return ZZZ_ERR_BADINP;
    return _zzz_entity_extra_acl((zzz_ctx_t*)ctx, ZZZ_EXTRA_NFSACL, (uint8_t*)acl, 0);
}

/**
 * Add OS/2 or POSIX 1003e std 17 ACL to the entity, may be called multiple times
 */
int zzz_entity_extra_acl_text(void *ctx, char *acl)
{
    if(!ctx || !acl || !*acl) return ZZZ_ERR_BADINP;
    return _zzz_entity_extra_acl((zzz_ctx_t*)ctx, ZZZ_EXTRA_TEXTACL, (uint8_t*)acl, 0);
}

/**
 * Add UUID ACL to the entity, may be called multiple times
 */
int zzz_entity_extra_acl_uuid(void *ctx, uint8_t *acl, uint16_t size)
{
    if(!ctx || !acl || !*acl || size < 16 || (size & 15)) return ZZZ_ERR_BADINP;
    return _zzz_entity_extra_acl((zzz_ctx_t*)ctx, ZZZ_EXTRA_UUIDACL, acl, size);
}

/**
 * Add OS specific tag to the entity, may be called multiple times
 */
int zzz_entity_extra_os_tag(void *ctx, int ostype, uint16_t tag, uint8_t *buf, uint16_t size)
{
    int i;
    zzz_ctx_t *zzz = (zzz_ctx_t*)ctx;
    if(!zzz || ostype < 0x10) return ZZZ_ERR_BADINP;
    if(zzz->entity.numextra && zzz->entity.extra[zzz->entity.numextra - 1].type == ostype) {
        i = zzz->entity.numextra - 1;
    } else {
        for(i = 0; i < zzz->entity.numextra && zzz->entity.extra[i].type != ostype; i++);
        if(i == zzz->entity.numextra) {
            zzz->entity.extra = (zzz_attr_t*)realloc(zzz->entity.extra, (zzz->entity.numextra + 1) * sizeof(zzz_attr_t));
            if(!zzz->entity.extra) return ZZZ_ERR_NOMEM;
            zzz->entity.extra[zzz->entity.numextra].type = ostype;
            zzz->entity.extra[zzz->entity.numextra].size = 0;
            zzz->entity.extra[zzz->entity.numextra].attr.any = NULL;
            zzz->entity.numextra++;
        }
    }
    if(!buf) size = 0;
    zzz->entity.extra[i].attr.any = (uint8_t*)realloc(zzz->entity.extra[i].attr.any,
        zzz->entity.extra[zzz->entity.numextra].size + size + 4);
    if(!zzz->entity.extra[i].attr.any) return ZZZ_ERR_NOMEM;
    memcpy(zzz->entity.extra[i].attr.any + zzz->entity.extra[i].size, &tag, 2);
    memcpy(zzz->entity.extra[i].attr.any + zzz->entity.extra[i].size + 2, &size, 2);
    if(buf && size)
        memcpy(zzz->entity.extra[i].attr.any + zzz->entity.extra[i].size + 4, buf, size);
    zzz->entity.extra[i].size += size + 4;
    return ZZZ_OK;
}

/**
 * Add file data to the entity, may be called multiple times (except for text files)
 */
int zzz_entity_data(void *ctx, uint8_t *buf, uint64_t size)
{
    ZSTD_inBuffer zi;
    ZSTD_outBuffer zo;
    int ret = ZZZ_OK, i, full;
    uint8_t *s;
#if NL == 2
    uint8_t *d, *buf2 = NULL;
#endif
    uint64_t remaining = 0;
    zzz_ctx_t *zzz = (zzz_ctx_t*)ctx;
    if(!zzz || !buf || size < 1 || ZZZ_FILE_TYPE(zzz->entity.header.type) != ZZZ_TYPE_REG) return ZZZ_ERR_BADINP;
    full = !zzz->pos && size < ZZZ_BUFSIZE && size == zzz->entity.header.uncompressed;
    if(!zzz->ee)
        zzz->entity.header.contentsize = zzz->entity.header.uncompressed;
    /* ASCII newline filter */
    if(ZZZ_NUM_FILTER(zzz->entity.header.type) > 0 && zzz->entity.filter[0].method == ZZZ_FILTER_ASCII) {
#if NL == 2
        buf2 = (uint8_t*)malloc(size);
        if(!buf2) return ZZZ_ERR_NOMEM;
        for(s = buf, d = buf2; s < buf + size; s++)
            if(*s == '\r') {
                size--;
            } else {
                if(*s == '\n' && s > buf && *(s - 1) != '\r') {
                    zzz->entity.header.uncompressed++;
                    remaining++;
                }
                if(d != s) *d = *s;
                d++;
            }
        buf = buf2;
#else
        for(s = buf; s < buf + size; s++)
            if(*s == '\n') {
                zzz->entity.header.uncompressed++;
                remaining++;
            }
#endif
    }
    zzz->entity.origcrc = crc32_z(zzz->entity.origcrc, buf, size);
    /* add compression filter */
    if(!zzz->pos) {
        zo.pos = 0;
        if(zzz->ee) {
            if(!zzz->fcmp) {
                zzz->fcmp = (uint8_t*)malloc(ZZZ_BUFSIZE);
                if(!zzz->fcmp) { ret = ZZZ_ERR_NOMEM; goto end; }
            }
            i = 1;
            if(full) {
                /* if we can compress the data in one round */
                zi.src = buf; zi.size = size; zi.pos = 0;
                zo.dst = zzz->fcmp; zo.size = ZZZ_BUFSIZE; zo.pos = 0;
                ZSTD_compressStream2(zzz->ee, &zo , &zi, ZSTD_e_end);
                if(zo.pos >= zzz->entity.header.uncompressed) {
                    i = 0; zo.pos = 0;
                    zzz->entity.header.contentsize = size;
                } else {
                    buf = zzz->fcmp;
                    zzz->entity.header.contentsize = size = zo.pos;
                }
            }
            if(i) {
                i = ZZZ_NUM_FILTER(zzz->entity.header.type);
                zzz->entity.filter[i].method = ZZZ_FILTER_ZSTD;
                zzz->entity.filter[i].level = 5;
                zzz->entity.header.type++;
            }
        }
        if((ret = _zzz_header(zzz)) != ZZZ_OK) goto end;
        if(full) {
            _zzz_write_enc(zzz, buf, size);
            zzz->pos += size;
            zzz->oneround = 1;
            goto end;
        }
    }
    zzz->pos += size + remaining;
    zzz->oneround = 0;
    if(zzz->ee && (zzz->entity.filter[0].method == ZZZ_FILTER_ZSTD || zzz->entity.filter[1].method == ZZZ_FILTER_ZSTD)) {
        zi.src = buf; zi.size = size; zi.pos = 0;
        zo.dst = zzz->fcmp; zo.size = ZZZ_BUFSIZE; zo.pos = 0;
        do {
            remaining = ZSTD_compressStream2(zzz->ee, &zo , &zi, zzz->pos >= zzz->entity.header.uncompressed ?
                ZSTD_e_end : ZSTD_e_continue);
            _zzz_write_enc(zzz, zzz->fcmp, zo.pos);
            zzz->entity.header.contentsize += zo.pos;
        } while(zzz->pos >= zzz->entity.header.uncompressed ? (remaining != 0) : (zi.pos != zi.size));
    } else {
        _zzz_write_enc(zzz, buf, size);
    }
end:
#if NL == 2
    if(buf2) free(buf2);
#endif
    return ret;
}

/**
 * Flush an entity to the archive
 */
int zzz_entity_flush(void *ctx)
{
    uint32_t crc;
    uint64_t s;
    int ret;
    zzz_ctx_t *zzz = (zzz_ctx_t*)ctx;
    if(!zzz) return ZZZ_ERR_BADINP;
    if(!zzz->pos && ((ret = _zzz_header(zzz)) != ZZZ_OK)) return ret;
    if(zzz->entity.target) {
        zzz->entity.origcrc = crc32_z(0, (uint8_t*)zzz->entity.target, zzz->entity.header.contentsize);
        if((ret = _zzz_write_enc(zzz, (uint8_t*)zzz->entity.target, zzz->entity.header.contentsize)) != ZZZ_OK) return ret;
        free(zzz->entity.target);
        zzz->entity.target = NULL;
    }
    if(zzz->chksiz && (ret = _zzz_write_enc(zzz, (uint8_t*)&zzz->chunk, zzz->chksiz)) != ZZZ_OK) return ret;
    if(!zzz->ae && !zzz->oneround) {
        if(zzz->fd != -1) return ZZZ_ERR_BADINP;
        if(zzz->mem) {
            memcpy(zzz->mem + zzz->hpos, zzz->hdr, sizeof(zzz_block_t));
        } else {
            zzz_fseek(zzz->f, zzz->hpos);
            zzz_fwrite(zzz->f, zzz->hdr, sizeof(zzz_block_t));
            zzz_fseek(zzz->f, zzz->apos);
        }
    }
    zzz->entity.blkcrc = crc32_z(0, zzz->hdr, zzz->hdrsize);
    zzz->entity.blkcrc = crc32_z(zzz->entity.blkcrc, (uint8_t*)zzz->entity.filename, zzz->entity.header.namelen & 0x7FFF);
    s = zzz->entity.header.contentsize;
    if(zzz->enc.passwdcrc && zzz->pad > 1)
        s = (s + zzz->pad - 1) & ~(zzz->pad - 1);
    zzz->entity.blkcrc = crc32_combine(zzz->entity.blkcrc, zzz->cmpcrc, s);
    crc = le32(zzz->entity.origcrc); if((ret = _zzz_write(zzz, &crc, 4)) != ZZZ_OK) return ret;
    zzz->entity.blkcrc = crc32_z(zzz->entity.blkcrc, (uint8_t*)&crc, 4);
    crc = le32(zzz->entity.blkcrc); if((ret = _zzz_write(zzz, &crc, 4)) != ZZZ_OK) return ret;
    zzz->end.acrc = crc32_combine(zzz->end.acrc, zzz->entity.blkcrc, zzz->hdrsize + (zzz->entity.header.namelen & 0x7FFF) + s + 4);
    zzz->end.acrc = crc32_z(zzz->end.acrc, (uint8_t*)&crc, 4);
    zzz->end.uncompressed += zzz->entity.header.uncompressed;
    if(zzz->ee) ZSTD_freeCCtx(zzz->ee);
    zzz->ee = NULL;
    return ZZZ_OK;
}

/**
 * Finish creating an archive
 */
int zzz_finish(void *ctx, uint8_t **mem, uint64_t *size)
{
    time_t t = time(NULL);
    struct tm *tm = gmtime(&t);
    zzz_ctx_t *zzz = (zzz_ctx_t*)ctx;
    if(!zzz) return ZZZ_ERR_BADINP;
    memcpy(&zzz->end.magic, ZZZ_MAGEND, 4);
    zzz->end.size = le16(sizeof(zzz_end_t));
    zzz->end.ayear = le16(tm->tm_year + 1900);
    zzz->end.amon = tm->tm_mon + 1;
    zzz->end.aday = tm->tm_mday;
    zzz->end.ahour = tm->tm_hour;
    zzz->end.amin = tm->tm_min;
    zzz->end.asec = tm->tm_sec;
    zzz->end.acrc = crc32_z(zzz->end.acrc, (uint8_t*)&zzz->end, sizeof(zzz_end_t) - 4);
#if __BYTE_ORDER != __LITTLE_ENDIAN
    zzz->end.filetypes = le16(zzz->end.filetypes);
    zzz->end.filters = le32(zzz->end.filters);
    zzz->end.uncompressed = le64(zzz->end.uncompressed);
    zzz->end.acrc = le32(zzz->end.acrc);
#endif
    _zzz_write(zzz, &zzz->end, sizeof(zzz_end_t));
    if(zzz->mem) {
        if(mem) { *mem = zzz->mem; } else { free(zzz->mem); }
    } else if(zzz->fd == -1) {
        zzz_fclose(zzz->f);
    }
    if(size) *size = zzz->apos + sizeof(zzz_end_t);
    _zzz_entity_free(&zzz->entity);
    if(zzz->hdr) free(zzz->hdr);
    if(zzz->fcmp) free(zzz->fcmp);
    if(zzz->cmp) free(zzz->cmp);
    if(zzz->ae) ZSTD_freeCCtx(zzz->ae);
    memset(ctx, 0, sizeof(zzz_ctx_t));
    free(zzz);
    return ZZZ_OK;
}

