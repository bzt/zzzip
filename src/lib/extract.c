/*
 * ZZZip/src/lib/extract.c
 *
 * Copyright (C) 2021 bzt (bztsrc@gitlab)
 *
 * Permission is hereby granted, free of charge, to any person
 * obtaining a copy of this software and associated documentation
 * files (the "Software"), to deal in the Software without
 * restriction, including without limitation the rights to use, copy,
 * modify, merge, publish, distribute, sublicense, and/or sell copies
 * of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
 * HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
 * WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 *
 * @brief ZZZip library archive extraction functions
 *
 */

#include "internal.h"

/**
 * Detect compression filter
 */
static int _zzz_detect(zzz_ctx_t *zzz, uint8_t *buf)
{
    uint8_t *hdr, c;
    uint16_t d;
    xz_crc32_init();
    if(!memcmp(buf, ZZZ_MAGIC, 4)) {
        return 0;
    }
    if(!zzz->cmp) {
        zzz->cmp = (uint8_t*)malloc(ZZZ_BUFSIZE);
        if(!zzz->cmp) return 0;
    }
    if(buf[0] == 0x1f && buf[1] == 0x8b) {
        /* gzip */
        hdr = buf + 3;
        c = *hdr++; hdr += 6;
        if(c & 4) { d = *hdr++; d |= (*hdr++ << 8); hdr += d; } if(hdr - buf > 16383) return -1;
        if(c & 8) { while(*hdr++ != 0); } if(hdr - buf > 16383) return -1;
        if(c & 16) { while(*hdr++ != 0); } if(hdr - buf > 16383) return -1;
        if(c & 2) { hdr += 2; } if(hdr - buf > 16383) return -1;
        if(inflateInit2(&zzz->az, -MAX_WBITS) != Z_OK) return -1;
        zzz->comp.method = ZZZ_FILTER_ZLIB;
        return (int)(hdr - buf);
    } else
    if(buf[0] == 'B' && buf[1] == 'Z' && buf[2] == 'h') {
        /* bzip2 */
        if(BZ2_bzDecompressInit(&zzz->ab, 0, 0) != BZ_OK) return -1;
        zzz->comp.method = ZZZ_FILTER_BZIP2;
        return 0;
    } else
    if(buf[0] == 0xFD && buf[1] == '7' && buf[2] == 'z' && buf[3] == 'X' && buf[4] == 'Z') {
        /* xz */
        zzz->ax = xz_dec_init(XZ_DYNALLOC, 1 << 26);
        if(!zzz->ax) return -1;
        zzz->comp.method = ZZZ_FILTER_XZ;
        return 0;
    } else
    if(buf[0] == 0x28 && buf[1] == 0xb5 && buf[2] == 0x2f && buf[3] == 0xFD) {
        /* zstd */
        zzz->ad = ZSTD_createDCtx();
        if(!zzz->ad) return -1;
        zzz->azi.src = zzz->cmp;
        zzz->comp.method = ZZZ_FILTER_ZSTD;
        return 0;
    }
    return -1;
}

/**
 * Read from source (file or memory)
 */
static int _zzz_read_source(zzz_ctx_t *zzz, uint8_t *buf, uint64_t size)
{
    if(zzz->mem) {
        memcpy(buf, zzz->mem + zzz->apos, size);
        zzz->apos += size;
        return size;
    } else if(zzz->fd != -1) {
        zzz->apos += size;
        return zzz_sread(zzz->fd, buf, size);
    } else {
        zzz->apos += size;
        return zzz_fread(zzz->f, buf, size);
    }
}

/**
 * Read from compressed archive
 */
static uint64_t _zzz_read(zzz_ctx_t *zzz, uint8_t *buf, uint64_t size)
{
    int ret = 0;
    uint64_t insiz = 0;
    switch(zzz->comp.method) {
        case ZZZ_FILTER_ZLIB:
            zzz->az.next_out = buf;
            zzz->az.avail_out = size;
            do {
                if(!zzz->az.avail_in) {
                    if(zzz->fd == -1) {
                        insiz = zzz->mpos - zzz->apos;
                        if(insiz < 1) { ret = Z_STREAM_END; break; }
                        if(insiz > ZZZ_BUFSIZE) insiz = ZZZ_BUFSIZE;
                    } else insize = ZZZ_BUFSIZE;
                    zzz->az.next_in = zzz->cmp;
                    zzz->az.avail_in = insiz;
                    if(!_zzz_read_source(zzz, zzz->cmp, insiz)) break;
                }
                ret = inflate(&zzz->az, Z_NO_FLUSH);
            } while(ret == Z_OK && zzz->az.avail_out > 0);
            insiz = ret == Z_OK || ret == Z_STREAM_END ? size - zzz->az.avail_out : 0;
            break;
        case ZZZ_FILTER_BZIP2:
            zzz->ab.next_out = (char*)buf;
            zzz->ab.avail_out = size;
            do {
                if(!zzz->ab.avail_in) {
                    if(zzz->fd == -1) {
                        insiz = zzz->mpos - zzz->apos;
                        if(insiz < 1) { ret = BZ_STREAM_END; break; }
                        if(insiz > ZZZ_BUFSIZE) insiz = ZZZ_BUFSIZE;
                    } else insize = ZZZ_BUFSIZE;
                    zzz->ab.next_in = (char*)zzz->cmp;
                    zzz->ab.avail_in = insiz;
                    if(!_zzz_read_source(zzz, zzz->cmp, insiz)) break;
                }
                ret = BZ2_bzDecompress(&zzz->ab);
            } while(ret == BZ_OK && zzz->ab.avail_out > 0);
            insiz = ret == BZ_OK || ret == BZ_STREAM_END ? size - zzz->ab.avail_out : 0;
            break;
        case ZZZ_FILTER_XZ:
            zzz->axd.out = buf;
            zzz->axd.out_pos = 0;
            zzz->axd.out_size = size;
            do {
                if(zzz->axd.in_pos == zzz->axd.in_size) {
                    if(zzz->fd == -1) {
                        insiz = zzz->mpos - zzz->apos;
                        if(insiz < 1) { ret = XZ_STREAM_END; break; }
                        if(insiz > ZZZ_BUFSIZE) insiz = ZZZ_BUFSIZE;
                    } else insize = ZZZ_BUFSIZE;
                    zzz->axd.in = zzz->cmp;
                    zzz->axd.in_pos = 0;
                    zzz->axd.in_size = insiz;
                    if(!_zzz_read_source(zzz, zzz->cmp, insiz)) break;
                }
                ret = xz_dec_run(zzz->ax, &zzz->axd);
                if(ret == XZ_UNSUPPORTED_CHECK) ret = XZ_OK;
            } while(ret == XZ_OK && zzz->axd.out_pos < zzz->axd.out_size);
            insiz = ret == XZ_OK || ret == XZ_STREAM_END ? zzz->axd.out_pos : 0;
            break;
        case ZZZ_FILTER_ZSTD:
            zzz->azo.dst = buf;
            zzz->azo.pos = 0;
            zzz->azo.size = size;
            do {
                if(zzz->azi.pos == zzz->azi.size) {
                    if(zzz->fd == -1) {
                        insiz = zzz->mpos - zzz->apos;
                        if(insiz < 1) { ret = 0; break; }
                        if(insiz > ZZZ_BUFSIZE) insiz = ZZZ_BUFSIZE;
                    } else insize = ZZZ_BUFSIZE;
                    zzz->azi.src = zzz->cmp;
                    zzz->azi.pos = 0;
                    zzz->azi.size = insiz;
                    if(!_zzz_read_source(zzz, zzz->cmp, insiz)) break;
                }
                ret = (int) ZSTD_decompressStream(zzz->ad, &zzz->azo, &zzz->azi);
            } while(!ZSTD_isError(ret) && zzz->azo.pos < zzz->azo.size);
            insiz = !ZSTD_isError(ret) ? zzz->azo.pos : 0;
            break;
        default:
            insiz = _zzz_read_source(zzz, buf, size);
            break;
    }
    if(insiz) {
        zzz->end.acrc = crc32_z(zzz->end.acrc, buf, insiz);
    }
    return insiz;
}

/**
 * Decrypt data
 */
static void _zzz_dec(zzz_ctx_t *zzz, uint8_t *buf, uint64_t size)
{
    int i;
    for(i = 0; i < zzz->enc.size; i++) {
        switch(zzz->enc.enctype[i]) {
            case ZZZ_ENC_AES_256_CBC:
                if(!buf || !size) { aes_reset(zzz, i); }
                else              { aes_dec(zzz, i, buf, size); }
                break;
            case ZZZ_ENC_SHA_256_XOR:
                if(!buf || !size) { sha_reset(zzz, i); }
                else              { sha_enc(zzz, i, buf, size); }
                break;
            default: break;
        }
    }
}

/**
 * Read potentially encrypted data
 */
static uint64_t _zzz_read_dec(zzz_ctx_t *zzz, uint8_t *buf, uint64_t size)
{
    uint64_t s = size;
    if(zzz->enc.passwdcrc && zzz->pad > 1)
        s = (size + zzz->pad - 1) & ~(zzz->pad - 1);
    s = _zzz_read(zzz, buf, s);
    zzz->entity.blkcrc = crc32_z(zzz->entity.blkcrc, buf, s);
    if(zzz->enc.passwdcrc)
        _zzz_dec(zzz, buf, s);
    zzz->pos += size;
    return s < size ? s : size;
}

/**
 * Open an archive. Fn is an UTF-8 filename.
 */
void *zzz_open_file(char *fn)
{
    int ret;
    uint8_t buf[16384];
    zzz_ctx_t *ctx = malloc(sizeof(zzz_ctx_t));
    if(!ctx) return NULL;
    memset(ctx, 0, sizeof(zzz_ctx_t));
    ctx->fd = -1;
    ctx->f = zzz_fopen(fn, 0);
    if(!ctx->f) {
        free(ctx);
        return NULL;
    }
    ctx->mpos = zzz_filesize(ctx->f);
    memset(buf, 0, sizeof(buf));
    zzz_fread(ctx->f, buf, sizeof(buf));
    if((ret = _zzz_detect(ctx, buf)) < 0) {
        zzz_fclose(ctx->f);
        free(ctx);
        return NULL;
    }
    if(ret > 0) zzz_fseek(ctx->f, ret);
    return (void*)ctx;
}

/**
 * Open an archive in a stream
 */
void *zzz_open_stream(int fd)
{
    uint8_t buf[16384];
    zzz_ctx_t *ctx = malloc(sizeof(zzz_ctx_t));
    if(!ctx) return NULL;
    memset(ctx, 0, sizeof(zzz_ctx_t));
    memset(buf, 0, sizeof(buf));
    ctx->fd = fd;
    zzz_sread(ctx->fd, buf, sizeof(buf));
    if(_zzz_detect(ctx, buf) != 0) {
        free(ctx);
        return NULL;
    }
    return (void*)ctx;
}

/**
 * Open an in-memory archive
 */
void *zzz_open_mem(uint8_t *mem, uint64_t size)
{
    int ret;
    zzz_ctx_t *ctx;
    if(!mem || size < sizeof(zzz_block_t) + sizeof(zzz_end_t)) return NULL;
    ctx = malloc(sizeof(zzz_ctx_t));
    if(!ctx) return NULL;
    memset(ctx, 0, sizeof(zzz_ctx_t));
    ctx->fd = -1;
    if((ret = _zzz_detect(ctx, mem)) < 0) {
        free(ctx);
        return NULL;
    }
    ctx->mem = mem + ret;
    ctx->mpos = size - ret;
    return (void*)ctx;
}

/**
 * Set decryption password
 */
int zzz_decrypt(void *ctx, uint8_t *password, int pwdlen)
{
    int i;
    uint32_t crc;
    zzz_ctx_t *zzz = (zzz_ctx_t*)ctx;
    if(!zzz || !zzz->enc.passwdcrc || !password || pwdlen < 1) return ZZZ_ERR_BADINP;
    crc = crc32_z(0, password, pwdlen);
    if(zzz->enc.passwdcrc != crc) return ZZZ_ERR_BADINP;
    for(i = 0; i < zzz->enc.size; i++)
        switch(zzz->enc.enctype[i]) {
            case ZZZ_ENC_AES_256_CBC: aes_init(zzz, password, pwdlen); break;
            case ZZZ_ENC_SHA_256_XOR: sha_init(zzz, password, pwdlen); break;
            default: return ZZZ_ERR_BADINP;
        }
    return ZZZ_OK;
}

/**
 * Read the next entity header from archive
 */
int zzz_read_header(void *ctx, zzz_entity_t **ent)
{
    int s, o, i, j, l, k, ret;
    uint32_t crc, blkcrc;
    zzz_meta_t *meta;
    zzz_block_t *hdr;
    zzz_enc_block_t *enc;
    zzz_ctx_t *zzz = (zzz_ctx_t*)ctx;
    if(!zzz) return ZZZ_ERR_BADINP;
    if(ent) *ent = NULL;
    if(!zzz->hdr)
        zzz->hdr = (uint8_t*)malloc(ZZZ_HEADER_MAXSIZE);
    if(!zzz->hdr) return ZZZ_ERR_NOMEM;
    if(!_zzz_read(zzz, zzz->hdr, 8)) return ZZZ_ERR_IO;
    if(!memcmp(zzz->hdr, ZZZ_MAGEND, 4)) {
        if(!_zzz_read(zzz, zzz->hdr + 8, sizeof(zzz_end_t) - 12)) return ZZZ_ERR_IO;
        crc = zzz->end.acrc;
        if(!_zzz_read(zzz, zzz->hdr + sizeof(zzz_end_t) - 4, 4)) return ZZZ_ERR_IO;
        return (zzz->end.uncompressed != le64(((zzz_end_t*)zzz->hdr)->uncompressed) ||
            zzz->end.numblk != le64(((zzz_end_t*)zzz->hdr)->numblk) || crc != le32(((zzz_end_t*)zzz->hdr)->acrc)) ?
                ZZZ_ERR_CORRUPT : ZZZ_END;
    }
    if(memcmp(zzz->hdr, ZZZ_MAGIC, 4)) return ZZZ_ERR_CORRUPT;
    hdr = (zzz_block_t*)zzz->hdr;
    enc = (zzz_enc_block_t*)zzz->hdr;
    s = (le16(hdr->size) | ((le16(hdr->namelen) & 0x8000) << 1));
    if(!_zzz_read(zzz, zzz->hdr + 8, s - 8)) return ZZZ_ERR_IO;
    if(!hdr->namelen) {
        zzz->enc.size = le16(hdr->size) - 13;
        zzz->enc.passwdcrc = le32(enc->passwdcrc);
        if(zzz->enc.size < 1 || zzz->enc.size > MAX_ENC_FILTERS || !enc->passwdcrc || enc->padding > 8)
            return ZZZ_ERR_CORRUPT;
        zzz->pad = (1 << enc->padding);
        for(o = 0; o < zzz->enc.size; o++) {
            if(enc->enctype[o] >= ZZZ_ENC_LAST)
                return ZZZ_ERR_UNSUPPORTED;
            zzz->enc.enctype[o] = enc->enctype[o];
        }
        zzz->end.numblk++;
        return ZZZ_ENCRYPTED;
    }
    if(ent) *ent = &zzz->entity;
    _zzz_entity_free(&zzz->entity);
    memcpy(&zzz->entity.header, zzz->hdr, sizeof(zzz_block_t));
    memcpy(&zzz->entity.filter, zzz->hdr + sizeof(zzz_block_t), ZZZ_NUM_FILTER(zzz->entity.header.type) * sizeof(zzz_filter_t));
    zzz->entity.header.namelen = le16(hdr->namelen) & 0x7FFF;
    zzz->entity.header.myear = le16(hdr->myear);
    zzz->entity.header.uncompressed = le64(hdr->uncompressed);
    zzz->entity.header.contentsize = le64(hdr->contentsize);
    zzz->entity.blkcrc = crc32_z(0, zzz->hdr, s);
    zzz->entity.origcrc = zzz->cmpcrc = 0;
    zzz->hpos = zzz->entity.header.contentsize;
    if(zzz->enc.passwdcrc && zzz->pad > 1)
        zzz->hpos = (zzz->hpos + zzz->pad - 1) & ~((uint64_t)(zzz->pad - 1));
    if((zzz->entity.filename = (char*)malloc(zzz->entity.header.namelen)) == NULL) return ZZZ_ERR_NOMEM;
    if(!_zzz_read(zzz, (uint8_t*)zzz->entity.filename, zzz->entity.header.namelen)) return ZZZ_ERR_IO;
    zzz->entity.blkcrc = crc32_z(zzz->entity.blkcrc, (uint8_t*)zzz->entity.filename, zzz->entity.header.namelen);
    if(ZZZ_FILE_TYPE(zzz->entity.header.type) >= ZZZ_TYPE_LAST) return ZZZ_ERR_UNSUPPORTED;
    o = sizeof(zzz_block_t) + ZZZ_NUM_FILTER(zzz->entity.header.type) * sizeof(uint16_t);
    if(zzz->enc.passwdcrc) {
        _zzz_dec(zzz, NULL, 0);
        /* must be decrypted before target because changes iv */
        if(s > o)
            _zzz_dec(zzz, zzz->hdr + o, s - o);
    }
    if(ZZZ_FILE_TYPE(zzz->entity.header.type) != ZZZ_TYPE_REG) {
        if(zzz->entity.header.contentsize) {
            if((zzz->entity.target = (char*)malloc(zzz->hpos)) == NULL) return ZZZ_ERR_NOMEM;
            if(!_zzz_read_dec(zzz, (uint8_t*)zzz->entity.target, zzz->hpos)) return ZZZ_ERR_IO;
            if(ZZZ_FILE_TYPE(zzz->entity.header.type) == ZZZ_TYPE_CHR || ZZZ_FILE_TYPE(zzz->entity.header.type) == ZZZ_TYPE_BLK) {
                zzz->entity.devmaj = le32(*((uint32_t*)(zzz->entity.target + 0)));
                zzz->entity.devmin = le32(*((uint32_t*)(zzz->entity.target + 4)));
            }
        }
        if(!_zzz_read(zzz, (uint8_t*)&crc, 4)) return ZZZ_ERR_IO;
        zzz->entity.blkcrc = crc32_z(zzz->entity.blkcrc, (uint8_t*)&crc, 4);
        zzz->entity.origcrc = le32(crc);
        crc = zzz->entity.blkcrc;
        if(!_zzz_read(zzz, (uint8_t*)&blkcrc, 4)) return ZZZ_ERR_IO;
        if(crc != le32(blkcrc)) return ZZZ_ERR_CORRUPT;
    } else
    if(ZZZ_NUM_FILTER(zzz->entity.header.type) > 0) {
        /* this should be an iteration, but for better performance we hardcode numfilters 0, 1 and 2 */
        switch(zzz->entity.filter[0].method) {
            case ZZZ_FILTER_ASCII: break;
            case ZZZ_FILTER_ZLIB:
                if(inflateInit2(&zzz->ez, -MAX_WBITS) != Z_OK) return ZZZ_ERR_NOMEM;
                zzz->ez.avail_in = 0;
                break;
            case ZZZ_FILTER_BZIP2:
                if(BZ2_bzDecompressInit(&zzz->eb, 0, 0) != BZ_OK) return ZZZ_ERR_NOMEM;
                zzz->eb.avail_in = 0;
                break;
            case ZZZ_FILTER_XZ:
                zzz->ex = xz_dec_init(XZ_DYNALLOC, 1 << 26);
                if(!zzz->ex) return ZZZ_ERR_NOMEM;
                zzz->exd.in_pos = zzz->exd.in_size = 0;
                break;
            case ZZZ_FILTER_ZSTD:
                zzz->ed = ZSTD_createDCtx();
                if(!zzz->ed) return ZZZ_ERR_NOMEM;
                zzz->ezi.pos = zzz->ezi.size = 0;
                break;
            default: return ZZZ_ERR_UNSUPPORTED;
        }
    }
    if(zzz->entity.target && zzz->entity.header.contentsize &&
        crc32_z(0, (uint8_t*)zzz->entity.target, zzz->entity.header.contentsize) != zzz->entity.origcrc) return ZZZ_ERR_CORRUPT;
    while(o < s) {
        l = (zzz->hdr[o + 3] << 8) | zzz->hdr[o + 2];
        switch(zzz->hdr[o]) {
            case ZZZ_EXTRA_PAD: break;
            /* parse these manually because we might need to allocate memory or convert between big-endian and little-endian */
            case ZZZ_EXTRA_META:
                zzz->entity.extra = (zzz_attr_t*)realloc(zzz->entity.extra, (zzz->entity.numextra + 1) * sizeof(zzz_attr_t));
                if(!zzz->entity.extra) return ZZZ_ERR_NOMEM;
                zzz->entity.extra[zzz->entity.numextra].type = ZZZ_EXTRA_META;
                zzz->entity.extra[zzz->entity.numextra].size = l - 4;
                zzz->entity.extra[zzz->entity.numextra].attr.meta.len = 0;
                zzz->entity.extra[zzz->entity.numextra].attr.meta.data = NULL;
                for(k = 4, i = 0; k < l; k += (zzz->hdr[o + k + 1] << 8) | zzz->hdr[o + k], i++);
                zzz->entity.extra[zzz->entity.numextra].attr.meta.data = meta = (zzz_meta_t*)malloc((i + 1) * sizeof(zzz_meta_t));
                if(!zzz->entity.extra[zzz->entity.numextra].attr.meta.data) return ZZZ_ERR_NOMEM;
                for(k = 4, i = 0; k < l; k += i, meta++) {
                    i = (zzz->hdr[o + k + 1] << 8) | zzz->hdr[o + k];
                    j = strlen((char*)zzz->hdr + o + k + 2) + 1;
                    meta->name = (char*)malloc(j);
                    if(!meta->name) return ZZZ_ERR_NOMEM;
                    memcpy(meta->name, zzz->hdr + o + k + 2, j);
                    meta->data = (uint8_t*)malloc(i - j - 2);
                    if(!meta->data) { free(meta->name); return ZZZ_ERR_NOMEM; }
                    memcpy(meta->data, zzz->hdr + o + k + j + 2, i - j - 2);
                    zzz->entity.extra[zzz->entity.numextra].attr.meta.len++;
                }
                zzz->entity.numextra++;
                break;
            case ZZZ_EXTRA_TIME:
                zzz->entity.extra = (zzz_attr_t*)realloc(zzz->entity.extra, (zzz->entity.numextra + 1) * sizeof(zzz_attr_t));
                if(!zzz->entity.extra) return ZZZ_ERR_NOMEM;
                memset(&zzz->entity.extra[zzz->entity.numextra], 0, sizeof(zzz_attr_t));
                zzz->entity.extra[zzz->entity.numextra].type = ZZZ_EXTRA_TIME;
                zzz->entity.extra[zzz->entity.numextra].size = l - 4;
                zzz->entity.extra[zzz->entity.numextra].attr.time.mtime = le64(*((uint64_t*)(zzz->hdr + o + 4)));
                if(l > 12)
                    zzz->entity.extra[zzz->entity.numextra].attr.time.atime = le64(*((uint64_t*)(zzz->hdr + o + 12)));
                if(l > 20)
                    zzz->entity.extra[zzz->entity.numextra].attr.time.ctime = le64(*((uint64_t*)(zzz->hdr + o + 20)));
                if(l > 28)
                    zzz->entity.extra[zzz->entity.numextra].attr.time.btime = le64(*((uint64_t*)(zzz->hdr + o + 28)));
                zzz->entity.numextra++;
                break;
            case ZZZ_EXTRA_ACCESS:
                zzz->entity.extra = (zzz_attr_t*)realloc(zzz->entity.extra, (zzz->entity.numextra + 1) * sizeof(zzz_attr_t));
                if(!zzz->entity.extra) return ZZZ_ERR_NOMEM;
                memset(&zzz->entity.extra[zzz->entity.numextra], 0, sizeof(zzz_attr_t));
                zzz->entity.extra[zzz->entity.numextra].type = ZZZ_EXTRA_ACCESS;
                zzz->entity.extra[zzz->entity.numextra].size = l - 4;
                zzz->entity.extra[zzz->entity.numextra].attr.access.mode = le32(*((uint32_t*)(zzz->hdr + o + 4)));
                zzz->entity.extra[zzz->entity.numextra].attr.access.uid = le64(*((uint64_t*)(zzz->hdr + o + 8)));
                zzz->entity.extra[zzz->entity.numextra].attr.access.gid = le64(*((uint64_t*)(zzz->hdr + o + 16)));
                k = strlen((char*)zzz->hdr + o + 24) + 1;
                zzz->entity.extra[zzz->entity.numextra].attr.access.user = (char*)malloc(k);
                if(!zzz->entity.extra[zzz->entity.numextra].attr.access.user) return ZZZ_ERR_NOMEM;
                memcpy(zzz->entity.extra[zzz->entity.numextra].attr.access.user, zzz->hdr + o + 24, k);
                zzz->entity.extra[zzz->entity.numextra].attr.access.group = (char*)malloc(l - k - 24);
                if(!zzz->entity.extra[zzz->entity.numextra].attr.access.group) {
                    free(zzz->entity.extra[zzz->entity.numextra].attr.access.user);
                    return ZZZ_ERR_NOMEM;
                }
                memcpy(zzz->entity.extra[zzz->entity.numextra].attr.access.group, zzz->hdr + o + 24 + k, l - k - 24);
                zzz->entity.numextra++;
                break;
            /* use the common handler for the rest */
            default:
                if((ret = zzz_entity_extra(zzz, (zzz->hdr[1] << 8) | zzz->hdr[0], zzz->hdr + o + 4, l)) != ZZZ_OK)
                    return ret;
                break;
        }
        o += l;
    }
    zzz->end.uncompressed += zzz->entity.header.uncompressed;
    zzz->end.numblk++;
    zzz->pos = 0;
    return ZZZ_OK;
}

/**
 * Read entity data from archive
 */
int zzz_read_data(void *ctx, uint8_t *buf, uint64_t *size)
{
    int ret = 0;
    uint8_t *buf2 = buf;
#if NL == 2
    uint8_t *d = buf;
#endif
    uint32_t crc;
    uint64_t insiz = 0, s;
    zzz_ctx_t *zzz = (zzz_ctx_t*)ctx;
    if(!zzz || !buf || !size || ZZZ_FILE_TYPE(zzz->entity.header.type) != ZZZ_TYPE_REG) return ZZZ_ERR_BADINP;
    if(zzz->pos > zzz->entity.header.contentsize) return ZZZ_END;
    if(zzz->pos == zzz->entity.header.contentsize) goto finish;
    /* this should be an iteration, but for better performance we hardcode numfilters 0, 1 and 2 */
    if(ZZZ_NUM_FILTER(zzz->entity.header.type) == 0
#if NL == 1
        || (ZZZ_NUM_FILTER(zzz->entity.header.type) == 1 && zzz->entity.filter[0].method == ZZZ_FILTER_ASCII)
#endif
    ) {
        insiz = zzz->entity.header.contentsize - zzz->pos;
        if(insiz > *size) insiz = *size;
        if(!(insiz = _zzz_read_dec(zzz, buf, insiz))) return ZZZ_ERR_IO;
        zzz->cmpcrc = crc32_z(zzz->cmpcrc, buf, insiz);
        goto finish;
    }
    if(ZZZ_NUM_FILTER(zzz->entity.header.type) > 0) {
        if(!zzz->fcmp) {
            zzz->fcmp = (uint8_t*)malloc(ZZZ_BUFSIZE);
            if(!zzz->fcmp) return ZZZ_ERR_NOMEM;
        }
        s = *size;
#if NL == 2
        if((ZZZ_NUM_FILTER(zzz->entity.header.type) == 1 && zzz->entity.filter[0].method == ZZZ_FILTER_ASCII) ||
            (ZZZ_NUM_FILTER(zzz->entity.header.type) == 2 && zzz->entity.filter[1].method == ZZZ_FILTER_ASCII)) {
            if(!zzz->nl) {
                zzz->nl = (uint8_t*)malloc(ZZZ_BUFSIZE);
                if(!zzz->nl) return ZZZ_ERR_NOMEM;
            }
            s /= 2;
            if(!s) s = 1;
            if(s > ZZZ_BUFSIZE) s = ZZZ_BUFSIZE;
            buf2 = zzz->nl;
        }
#endif
        switch(zzz->entity.filter[0].method) {
#if NL == 2
            case ZZZ_FILTER_ASCII:
                insiz = zzz->entity.header.contentsize - zzz->pos;
                if(insiz > s) insiz = s;
                if(!(insiz = _zzz_read_dec(zzz, buf2, insiz))) return ZZZ_ERR_IO;
                if(zzz->pos + insiz > zzz->entity.header.contentsize) insiz = zzz->entity.header.contentsize - zzz->pos;
                zzz->cmpcrc = crc32_z(zzz->cmpcrc, buf2, insiz);
                goto nlconv;
#endif
            case ZZZ_FILTER_ZLIB:
                zzz->ez.next_out = buf2;
                zzz->ez.avail_out = s;
                do {
                    if(!zzz->ez.avail_in) {
                        insiz = zzz->entity.header.contentsize - zzz->pos;
                        if(insiz < 1) { ret = Z_STREAM_END; break; }
                        if(insiz > ZZZ_BUFSIZE) insiz = ZZZ_BUFSIZE;
                        zzz->ez.next_in = zzz->fcmp;
                        zzz->ez.avail_in = insiz;
                        if(!(insiz = _zzz_read_dec(zzz, zzz->fcmp, insiz))) return ZZZ_ERR_IO;
                        zzz->pos += insiz;
                    }
                    ret = inflate(&zzz->ez, Z_NO_FLUSH);
                } while(ret == Z_OK && zzz->ez.avail_out > 0);
                insiz = ret == Z_OK || ret == Z_STREAM_END ? s - zzz->ez.avail_out : 0;
                break;
            case ZZZ_FILTER_BZIP2:
                zzz->eb.next_out = (char*)buf2;
                zzz->eb.avail_out = s;
                do {
                    if(!zzz->eb.avail_in) {
                        insiz = zzz->entity.header.contentsize - zzz->pos;
                        if(insiz < 1) { ret = BZ_STREAM_END; break; }
                        if(insiz > ZZZ_BUFSIZE) insiz = ZZZ_BUFSIZE;
                        zzz->eb.next_in = (char*)zzz->fcmp;
                        zzz->eb.avail_in = insiz;
                        if(!(insiz = _zzz_read_dec(zzz, zzz->fcmp, insiz))) return ZZZ_ERR_IO;
                    }
                    ret = BZ2_bzDecompress(&zzz->eb);
                } while(ret == BZ_OK && zzz->eb.avail_out > 0);
                insiz = ret == BZ_OK || ret == BZ_STREAM_END ? s - zzz->eb.avail_out : 0;
                break;
            case ZZZ_FILTER_XZ:
                zzz->exd.out = buf2;
                zzz->exd.out_pos = 0;
                zzz->exd.out_size = s;
                do {
                    if(zzz->exd.in_pos == zzz->exd.in_size) {
                        insiz = zzz->entity.header.contentsize - zzz->pos;
                        if(insiz < 1) { ret = XZ_STREAM_END; break; }
                        if(insiz > ZZZ_BUFSIZE) insiz = ZZZ_BUFSIZE;
                        zzz->exd.in = zzz->fcmp;
                        zzz->exd.in_pos = 0;
                        zzz->exd.in_size = insiz;
                        if(!(insiz = _zzz_read_dec(zzz, zzz->fcmp, insiz))) return ZZZ_ERR_IO;
                    }
                    ret = xz_dec_run(zzz->ax, &zzz->exd);
                    if(ret == XZ_UNSUPPORTED_CHECK) ret = XZ_OK;
                } while(ret == XZ_OK && zzz->exd.out_pos < zzz->exd.out_size);
                insiz = ret == XZ_OK || ret == XZ_STREAM_END ? zzz->exd.out_pos : 0;
                break;
            case ZZZ_FILTER_ZSTD:
                zzz->ezo.dst = buf2;
                zzz->ezo.pos = 0;
                zzz->ezo.size = s;
                do {
                    if(zzz->ezi.pos == zzz->ezi.size) {
                        insiz = zzz->entity.header.contentsize - zzz->pos;
                        if(insiz < 1) { ret = 0; break; }
                        if(insiz > ZZZ_BUFSIZE) insiz = ZZZ_BUFSIZE;
                        zzz->ezi.src = zzz->fcmp;
                        zzz->ezi.pos = 0;
                        zzz->ezi.size = insiz;
                        if(!(insiz = _zzz_read_dec(zzz, zzz->fcmp, insiz))) return ZZZ_ERR_IO;
                    }
                    ret = (int) ZSTD_decompressStream(zzz->ed, &zzz->ezo, &zzz->ezi);
                } while(!ZSTD_isError(ret) && zzz->ezo.pos < zzz->ezo.size);
                insiz = !ZSTD_isError(ret) ? zzz->ezo.pos : 0;
                break;
            default:
                return ZZZ_ERR_CORRUPT;
        }
    }
    zzz->cmpcrc = crc32_z(zzz->cmpcrc, buf2, insiz);
#if NL == 2
    if(ZZZ_NUM_FILTER(zzz->entity.header.type) > 1 && zzz->entity.filter[1].method == ZZZ_FILTER_ASCII) {
nlconv:
        for(s = 0; s < insiz; s++) {
            if(*buf2 == '\n')
                *d++ = '\r';
            *d++ = *buf2++;
        }
        insiz = d - buf;
    }
#endif
finish:
    *size = insiz;
    if(zzz->pos >= zzz->entity.header.contentsize) {
        if(ZZZ_NUM_FILTER(zzz->entity.header.type) > 0)
            switch(zzz->entity.filter[0].method) {
                case ZZZ_FILTER_ZLIB: inflateEnd(&zzz->ez); break;
                case ZZZ_FILTER_BZIP2: BZ2_bzDecompressEnd(&zzz->eb); break;
                case ZZZ_FILTER_XZ: xz_dec_end(zzz->ex); break;
                case ZZZ_FILTER_ZSTD: ZSTD_freeDCtx(zzz->ed); break;
            }
        if(!_zzz_read(zzz, (uint8_t*)&crc, 4)) return ZZZ_ERR_IO;
        zzz->entity.blkcrc = crc32_z(zzz->entity.blkcrc, (uint8_t*)&crc, 4);
        zzz->entity.origcrc = le32(crc);
        if(!_zzz_read(zzz, (uint8_t*)&crc, 4)) return ZZZ_ERR_IO;
        if(zzz->entity.blkcrc != le32(crc) || zzz->cmpcrc != zzz->entity.origcrc) return ZZZ_ERR_CORRUPT;
        zzz->pos += 8;
    }
    return ZZZ_OK;
}

/**
 * Close the archive and free resources
 */
int zzz_close(void *ctx)
{
    zzz_ctx_t *zzz = (zzz_ctx_t*)ctx;
    if(!zzz) return ZZZ_ERR_BADINP;
    _zzz_entity_free(&zzz->entity);
    if(!zzz->mem && zzz->fd == -1)
        zzz_fclose(zzz->f);
    if(zzz->hdr) free(zzz->hdr);
    if(zzz->nl) free(zzz->nl);
    if(zzz->fcmp) free(zzz->fcmp);
    if(zzz->cmp) free(zzz->cmp);
    if(zzz->comp.method == ZZZ_FILTER_ZLIB) inflateEnd(&zzz->az);
    if(zzz->comp.method == ZZZ_FILTER_BZIP2) BZ2_bzDecompressEnd(&zzz->ab);
    if(zzz->ax) xz_dec_end(zzz->ax);
    if(zzz->ad) ZSTD_freeDCtx(zzz->ad);
    memset(ctx, 0, sizeof(zzz_ctx_t));
    free(zzz);
    return ZZZ_OK;
}

