/*
 * ZZZip/src/lib/common.c
 *
 * Copyright (C) 2021 bzt (bztsrc@gitlab)
 *
 * Permission is hereby granted, free of charge, to any person
 * obtaining a copy of this software and associated documentation
 * files (the "Software"), to deal in the Software without
 * restriction, including without limitation the rights to use, copy,
 * modify, merge, publish, distribute, sublicense, and/or sell copies
 * of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
 * HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
 * WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 *
 * @brief ZZZip library common functions to archive cration and extraction
 *
 */

#include "internal.h"
#ifndef __WIN32__
#include <sys/stat.h>
extern int fileno(FILE *f);
extern char *realpath (const char *__name, char *__resolved);
extern size_t readlink (const char *__path, char *__buf, size_t __len);
extern int symlink(const char *__path1, const char *__path2);
#endif

/**
 * Open a file for writing
 */
zzz_file_t zzz_fopen(char *fn, int wr)
{
    return (zzz_file_t)fopen(fn, wr ? "wb" : "rb");
}

/**
 * Write to a file
 */
int64_t zzz_fwrite(zzz_file_t f, void *buf, int64_t size)
{
    return fwrite(buf, size, 1, f) ? size : 0;
}

/**
 * Read from a file
 */
int64_t zzz_fread(zzz_file_t f, void *buf, int64_t size)
{
    return fread(buf, size, 1, f) ? size : 0;
}

/**
 * Seek in a file
 */
int zzz_fseek(zzz_file_t f, int64_t offs)
{
#if !defined(__WIN32__) && !defined(MACOS)
    fpos_t pos = {0};
    pos.__pos = offs;
#else
    fpos_t pos = (fpos_t)offs;
#endif
    return fsetpos(f, &pos);
}

/**
 * Return file size, or -1 if file desn't exists
 */
uint64_t zzz_filesize(zzz_file_t f)
{
#ifndef __WIN32__
    struct stat st;
    st.st_size = 0;
    fstat(fileno(f), &st);
    return (uint64_t)st.st_size;
#else
    return (uint64_t)_filelengthi64(_fileno(f));
#endif
}

/**
 * Close file
 */
void zzz_fclose(zzz_file_t f)
{
    fclose(f);
}

/**
 * Write to stream
 */
int64_t zzz_swrite(int fd, void *buf, int64_t size)
{
    return (int64_t)write(fd, buf, (size_t)size);
}

/**
 * Read from stream
 */
int64_t zzz_sread(int fd, void *buf, int64_t size)
{
    return (int64_t)read(fd, buf, (size_t)size);
}

/**
 * Get canonized file name
 */
char *zzz_realpath(const char *path)
{
#ifdef __WIN32__
    wchar_t *buf, *buf2;
    char *buf3;
    int wlen;
    wlen = MultiByteToWideChar(CP_UTF8, 0, path, strlen(path), NULL, 0);
    if(wlen < 1) return NULL;
    buf = (wchar_t*)malloc((wlen + 1) * sizeof(wchar_t));
    if(!buf) return NULL;
    MultiByteToWideChar(CP_UTF8, 0, path, strlen(path), buf, wlen);
    buf[wlen] = 0;
    buf2 = (wchar_t*)malloc(65536);
    if(!buf2) { free(buf); return NULL; }
    buf3 = (char*)malloc(32768);
    if(!buf3) { free(buf); free(buf2); return NULL; }
    wlen = GetFullPathNameW(buf, wlen, buf2, NULL);
    wlen = WideCharToMultiByte(CP_UTF8, 0, buf2, wlen, buf3, 32767, NULL, NULL);
    buf3[wlen] = 0;
    free(buf);
    free(buf2);
    return buf3;
#else
    return realpath(path, NULL);
#endif
}

/**
 * Free an entity record from memory
 */
void _zzz_entity_free(zzz_entity_t *ent)
{
    int i, j;
    if(ent->filename) free(ent->filename);
    if(ent->target) free(ent->target);
    if(ent->extra) {
        for(i = 0; i < ent->numextra; i++) {
            switch(ent->extra[i].type) {
                case ZZZ_EXTRA_META:
                    if(ent->extra[i].attr.meta.data) {
                        for(j = 0; j < ent->extra[i].attr.meta.len; j++) {
                            if(ent->extra[i].attr.meta.data[j].name) free(ent->extra[i].attr.meta.data[j].name);
                            if(ent->extra[i].attr.meta.data[j].data) free(ent->extra[i].attr.meta.data[j].data);
                        }
                        free(ent->extra[i].attr.meta.data);
                    }
                    break;
                case ZZZ_EXTRA_TIME:
                    break;
                case ZZZ_EXTRA_ACCESS:
                    if(ent->extra[i].attr.access.user) free(ent->extra[i].attr.access.user);
                    if(ent->extra[i].attr.access.group) free(ent->extra[i].attr.access.group);
                    break;
                default:
                    if(ent->extra[i].attr.any) free(ent->extra[i].attr.any);
                    break;
            }
        }
        free(ent->extra);
    }
    memset(ent, 0, sizeof(zzz_entity_t));
}
void zzz_entity_free(zzz_entity_t *ent)
{
    if(!ent) return;
    _zzz_entity_free(ent);
    free(ent);
}

/**
 * Add an arbitrary extra field to the entity
 */
int zzz_entity_extra(void *ctx, int type, uint8_t *buf, uint16_t size)
{
    zzz_ctx_t *zzz = (zzz_ctx_t*)ctx;
    if(!zzz || !buf || !size) return ZZZ_ERR_BADINP;
    zzz->entity.extra = (zzz_attr_t*)realloc(zzz->entity.extra, (zzz->entity.numextra + 1) * sizeof(zzz_attr_t));
    if(!zzz->entity.extra) return ZZZ_ERR_NOMEM;
    zzz->entity.extra[zzz->entity.numextra].attr.any = (uint8_t*)malloc(size);
    if(!zzz->entity.extra[zzz->entity.numextra].attr.any) return ZZZ_ERR_NOMEM;
    zzz->entity.extra[zzz->entity.numextra].type = type & 0xFF;
    zzz->entity.extra[zzz->entity.numextra].rev = type >> 8;
    zzz->entity.extra[zzz->entity.numextra].size = size;
    memcpy(zzz->entity.extra[zzz->entity.numextra].attr.any, buf, size);
    zzz->entity.numextra++;
    return ZZZ_OK;
}

